from kivy.app import App
from kivy.lang import Builder
from kivy.uix.label import Label
from kivy.properties import StringProperty, NumericProperty, BooleanProperty
from kivy.uix.textinput import TextInput
import re
Builder.load_string('''
<N4Label>
    canvas.before:
        Color:
            rgba: app.colors['red_test'] if root.debug else [0, 0, 0, 0]
        Rectangle:
            size: self.size
            pos: self.pos
    size_hint: None, None
    size: self.texture_size if self.opacity > 0 else (0, 0)
    font: './assets/fonts/' + root. font_name
    color: app.colors['white']
    canvas:
        Color:
            rgba: 0, 0, 0, 0
        Rectangle:
            size: self.size
            pos: self.pos
''')

class N4Label(Label):
    debug = BooleanProperty(False)
    enabled = BooleanProperty(True)
    configs = {
        'body':{
            'font_name': './assets/fonts/Montserrat-Regular.ttf',
            'font_size': 13,
        },
    }

    style = StringProperty('body')
    font_name = StringProperty('./assets/fonts/Montserrat-Regular.ttf')

    def __init__(self, **kw):
        super(N4Label, self).__init__(**kw)
        self.on_style(None, self.style)

    def on_style(self, instance, style):
        for attr in self.configs[style]:
            setattr(self, attr, self.configs[style][attr])

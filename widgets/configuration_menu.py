import kivy
from kivy.lang import Builder
from kivy.app import App
from kivy.uix.relativelayout import RelativeLayout
from kivy.animation import Animation

Builder.load_string('''
<ConfigurationMenu>
    Header:
        id: header
        pos: 0, root.height - self.height
    N4Label:
        id: arq_lbl
        text: 'ARQUIVO:'
        pos: 30, 420
        color: app.colors['black']
    N4LargeTextInput:
        id: arq_txt
        text: 'Back_In_Black'
        pos: arq_lbl.x + arq_lbl.width + 20, int(arq_lbl.center_y - self.height/2)

    N4Label:
        text: 'INPUT:'
        pos: 30, 380
        color: app.colors['black']
    N4RadioButton
        id: input_1
        group: 'input'
        pos: 40, 348
    N4Label:
        text: 'Renderização'
        pos: input_1.x + input_1.width + 5, input_1.y + self.height / 2.0
        color: app.colors['black']
    N4RadioButton
        id: input_2
        group: 'input'
        pos: 40, 316
        state: 'down'
    N4Label:
        text: 'Tempo real'
        pos: input_2.x + input_2.width + 5, input_2.y + self.height / 2.0
        color: app.colors['black']
    N4RadioButton
        id: input_3
        group: 'input'
        pos: 40, 282
    N4Label:
        text: 'Microfone'
        pos: input_3.x + input_3.width + 5, input_3.y + self.height - 1
        color: app.colors['black']

    N4Label:
        text: 'ESCALA:'
        pos: 225, 380
        color: app.colors['black']

    N4RadioButton
        id: escala_1
        group: 'escala'
        pos: 235, 348
        state: 'down'
        on_state:
            root.save()
    N4Label:
        text: 'Linear'
        pos: escala_1.x + escala_1.width + 5, escala_1.y + self.height - 1
        color: app.colors['black']
    N4RadioButton
        id: escala_2
        group: 'escala'
        pos: 235, 316
        on_state:
            root.save()
    N4Label:
        text: 'Logarítmica'
        pos: escala_2.x + escala_2.width + 5, escala_2.y + self.height / 2.0
        color: app.colors['black']

    N4Label:
        id: fil_lbl
        text: 'FILTRAGEM:'
        pos: 30, 245
        color: app.colors['black']
    N4TextInput:
        id: fil_txt
        text: '5'
        pos: fil_lbl.x + fil_lbl.width + 20, int(fil_lbl.center_y - self.height/2)
        on_text_validate:
            root.save()

    N4Label:
        id: esp_lbl
        text: 'ESPAÇAMENTO:'
        pos: 30, 200
        color: app.colors['black']
    N4TextInput:
        id: esp_txt
        text: '2'
        pos: esp_lbl.x + esp_lbl.width + 20, int(esp_lbl.center_y - self.height/2)
        on_text_validate:
            root.save()

    N4Label:
        id: lar_lbl
        text: 'LARGURA:'
        pos: 30, 155
        color: app.colors['black']
    N4TextInput:
        id: lar_txt
        text: '1'
        pos: lar_lbl.x + lar_lbl.width + 20, int(lar_lbl.center_y - self.height/2)
        on_text_validate:
            root.save()

    N4Button:
        text: 'PRONTO'
        size: 120, 55
        pos: 260, 0
        center_y: header.center_y
        color: app.colors['black']
        hover_enabled: False
        on_press:
            root.save()
            root.hide()

<Header@RelativeLayout>
    size_hint: 1, None
    height: 50
    canvas:
        Color:
            rgba: app.colors['off_white']
        Rectangle:
            size: self.size
            pos: 0, 0
    N4Label:
        text: 'CONFIGURAÇÕES'
        color: app.colors['black']
        pos: 20, 16
    ''')

class ConfigurationMenu(RelativeLayout):
    def save(self):
        app = App.get_running_app()
        app.song_name = self.ids.arq_txt.text
        app.chunk_size = 1024
        app.filter_ammount = int(self.ids.fil_txt.text)
        app.bar_spacing = int(self.ids.esp_txt.text)
        app.bar_width = int(self.ids.lar_txt.text)
        app.input_type = [self.ids.input_1.state == 'down', self.ids.input_2.state == 'down', self.ids.input_3.state == 'down', ]
        app.scale_type = [self.ids.escala_1.state == 'down', self.ids.escala_2.state == 'down']

    def hide(self):
        Animation(x=-400, d=0.5, t='out_quad').start(self)

    def show(self):
        Animation(x=0, d=0.5, t='out_quad').start(self)

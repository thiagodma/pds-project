from kivy.app import App
from kivy.lang import Builder
from kivy.uix.checkbox import CheckBox
Builder.load_string('''
<N4RadioButton>
    size_hint: None, None
    size: 32, 32
    background_radio_normal: './assets/img/radio_unselected.png'
    background_radio_down: './assets/img/radio_selected.png'

''')

class N4RadioButton(CheckBox):
    def __init__(self, **kw):
        super(N4RadioButton, self).__init__(**kw)

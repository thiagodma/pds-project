from kivy.app import App
from kivy.lang import Builder
from kivy.uix.textinput import TextInput
from kivy.properties import NumericProperty, ListProperty, BooleanProperty
Builder.load_string('''
<N4TextInput>
    size_hint: None, None
    size: 50, 25
    multiline: False
    background_active: './assets/img/textinput_bg.png'
    background_normal: './assets/img/textinput_bg.png'
    font_name: './assets/fonts/Montserrat-Regular.ttf'
    selection_color: [0.2, 0.2, 0.2, 0.2]
    cursor_color: app.colors['black']
    padding: [9, 4, 0, 0]
    font_size: 13
    color: app.colors['black']

<N4LargeTextInput>
    size_hint: None, None
    size: 173, 25
    multiline: False
    background_active: './assets/img/textinput_large_bg.png'
    background_normal: './assets/img/textinput_large_bg.png'
    font_name: './assets/fonts/Montserrat-Regular.ttf'
    selection_color: [0.2, 0.2, 0.2, 0.2]
    cursor_color: app.colors['black']
    padding: [9, 4, 0, 0]
    font_size: 13
    color: app.colors['black']
''')

class N4TextInput(TextInput):
    def __init__(self, **kw):
        super(N4TextInput, self).__init__(**kw)

class N4LargeTextInput(TextInput):
    def __init__(self, **kw):
        super(N4LargeTextInput, self).__init__(**kw)

from kivy.app import App
from kivy.lang import Builder
from kivy.uix.label import Label
from kivy.properties import NumericProperty, ListProperty, BooleanProperty
from widgets.n4label import N4Label
from kivy.uix.behaviors import ButtonBehavior
Builder.load_string('''
<N4Button>
    canvas.before:
        Color:
            rgba: ([1, 1, 1, root.hover_opacity] if root.hover_color == [0, 0, 0, 0] else root.hover_color) if root.hover else [1, 1, 1, 0]
        Rectangle:
            size: self.size
            pos: self.pos
    style: 'body'
    size_hint: None, None
    valign: 'center'
    halign: 'center'
    size: (100, 100) if self.opacity > 0 else (0, 0)
    text_size: self.size
''')

class N4Button(ButtonBehavior, N4Label):
    hover = BooleanProperty(False)
    hover_enabled = BooleanProperty(True)
    hover_color = ListProperty([0, 0, 0, 0])
    hover_opacity = NumericProperty(0.15)
    def __init__(self, **kw):
        from kivy.core.window import Window
        Window.bind(mouse_pos=self.on_mouse_pos)
        super(N4Button, self).__init__(**kw)

    def on_press(self):
        if not self.enabled:
            return
        print('[ON PRESS] dispatched on', self)
        return super(N4Button, self).on_press()

    def on_mouse_pos(self, *args):
        pos = args[1]
        wpos = self.to_window(*self.pos)
        collide_x =  wpos[0] < pos[0] < wpos[0] + self.width
        collide_y =  wpos[1] < pos[1] < wpos[1] + self.height
        self.hover = collide_x and collide_y and self.enabled and self.hover_enabled

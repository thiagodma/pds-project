from kivy.app import App
from kivy.lang import Builder
from kivy.properties import StringProperty, NumericProperty, BooleanProperty
from kivy.uix.slider import Slider
from numpy import hamming, hanning, blackman
from scipy import signal
from time import time
from math import ceil
Builder.load_string('''
<Filter>
    opacity: 1 if app.playing else 0
    size_hint: None, None
    size: 200, 15
    cursor_image: './assets/img/filter.png'
    hover_enabled: False
    max: 10
    min: -10
    N4Label:
        id: range
        center_y: root.center_y
        x: root.x + root.width + 10
        color: app.colors['black']
        text: root.name
    N4Label:
        center_y: root.center_y
        x: root.x - self.width - 10
        color: app.colors['black']
        text: '{}{:.2f} dB'.format('+' if root.gain >=0 else '', root.gain)
''')

class Filter(Slider):
    """
    Essa é a classe que você deve reimplementar.
    """
    low_freq = NumericProperty(0)
    high_freq = NumericProperty(0)
    name = StringProperty('')
    gain = NumericProperty(0)
    def __init__(self, **kw):
        super(Filter, self).__init__(**kw)

    def on_value(self, instance, value):
        self.gain = - value

    def on_low_freq(self, instance, value):
        self.name = '{}'.format(self.low_freq)
        self.update_window()

    def on_high_freq(self, instance, value):
        self.name = '{}'.format(self.low_freq)
        self.update_window()

    def update_window(self):
        from kivy.app import App
        wtype = 'rectangular'
        size = int(ceil((self.high_freq - self.low_freq)/43.0))

        self.window = [0] * int(ceil(self.low_freq/43.0))
        if wtype == 'rectangular':
            self.window = self.window + [1 for i in range(size)]
        elif wtype == 'hanning':
            self.window = self.window + [n for n in hanning(size)]
        elif wtype == 'hamming':
            self.window = self.window + [n for n in hamming(size)]
        elif wtype == 'blackman':
            self.window = self.window + [n for n in blackman(size)]
        self.window = self.window + [0] * (1024 - int(ceil(self.high_freq/43.0)))

    def get_windowed_value(self, sample_index):
        """
        Método que faz a filtragem meio fajuta... Tá usando 'ifs' pra selecionar a região
        do espectro que deseja alterar
        """
        if sample_index * 43 < self.low_freq:
            return 0
        elif sample_index * 43 > self.high_freq:
            return 0
        else:
            return 10 ** (self.gain/20.0) * self.window[sample_index]

    def apply(self, fft_data, frequencies):
        for data, freq in zip(fft_data, frequencies):
            print(data, freq)

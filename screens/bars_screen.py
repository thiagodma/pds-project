import kivy
from kivy.lang import Builder
from kivy.uix.screenmanager import Screen
from kivy.properties import ObjectProperty, NumericProperty
from widgets.filter import Filter

Builder.load_string('''
<BarsScreen>
    rl: rl
    RelativeLayout:
        id: rl
        size_hint: 1, 1
    StackLayout:
        id: sl
        size_hint: 0.94, None
        pos: 0, 125
        height: root.height - 175
        spacing: 20
        orientation: 'bt-rl'
        Filter:
            low_freq: 0
            high_freq: 200

        Filter:
            low_freq: 200
            high_freq: 400

        Filter:
            low_freq: 400
            high_freq: 800

        Filter:
            low_freq: 800
            high_freq: 1600

        Filter:
            low_freq: 1600
            high_freq: 3200

        Filter:
            low_freq: 3200
            high_freq: 6400

        Filter:
            low_freq: 6400
            high_freq: 12800

        Filter:
            low_freq: 12800
            high_freq: 44100

    N4ImageButton:
        source: './assets/img/menu.png'
        pos: 27, root.height - 38
        on_press:
            app.root.ids.menu.show()
    N4ImageButton:
        source: './assets/img/play_btn.png'
        pos: root.width - self.width - 27, root.height - self.height - 13
        hover_enabled: False
        on_press:
            app.toggle_player()
            self.source = './assets/img/pause_btn.png' if self.source == './assets/img/play_btn.png' else './assets/img/play_btn.png'
    ''')

class BarsScreen(Screen):
    filter_spacing = NumericProperty(80)
    rl = ObjectProperty(None, allownone=True)
    bars = []

    def get_filters(self):
        return self.ids.sl.children

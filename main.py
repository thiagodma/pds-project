import kivy
from kivy.app import App
from kivy.uix.relativelayout import RelativeLayout
from kivy.lang import Builder
from kivy.uix.widget import Widget
from kivy.uix.screenmanager import Screen
from kivy.properties import BooleanProperty, NumericProperty, ObjectProperty, ListProperty, DictProperty
from kivy.clock import mainthread

# Custom widgets imports
from screens.bars_screen import BarsScreen
from widgets.filter import Filter
from widgets.configuration_menu import ConfigurationMenu
from widgets.n4label import N4Label
from widgets.n4image import N4Image
from widgets.n4imagebutton import N4ImageButton
from widgets.n4button import N4Button
from widgets.n4radiobutton import N4RadioButton
from widgets.n4textinput import N4TextInput, N4LargeTextInput

# Audio management imports
from scipy.fftpack import rfft, fft
import pyaudio
import wave
from pydub import AudioSegment
import struct

# Other modules
from threading import Thread
import numpy as np
import os
import gzip
import pickle
import time
Builder.load_string('''
<PDSRoot>
    canvas:
        Color:
            rgba: app.colors['off_white']
        Rectangle:
            size: self.size
            pos: self.pos
    Widget:
        id: progress_bar
        size_hint: app.progress, None
        height: 50
        canvas:
            Color:
                rgba: 1, 0, 0, 1
            Rectangle:
                size: self.size
                pos: self.pos
    ScreenManager:
        id: screen_manager
        BarsScreen:
            id: bars_screen
            name: 'bars_screen'
    ConfigurationMenu:
        id: menu
        size_hint: None, 1
        width: 400

<Bar>
    size_hint: None, 1
    canvas:
        Color:
            rgba: 1, 0, 0, 1
        Rectangle:
            size: self.width, root.value
            pos: self.pos
''')


from kivy.config import Config
Config.set('graphics', 'height', '512')
Config.set('graphics', 'width', '400')

from kivy.core.window import Window

def get_fft(data):
    window_data = True
    if window_data:
        data = data * np.hamming(len(data))
    fft = data
    fft = np.fft.fft(fft)
    fft = fft / 1000
    return fft

def get_inverse_fft(fft_data):
    window_data = True
    fft_data = 1000 * fft_data
    data = fft_data
    data = np.fft.ifft(data)
    if window_data:
        data = data / np.hamming(len(data))
    # import pdb; pdb.set_trace()
    return np.real(data).astype(int)

def fft_to_bar(data, width=1, filter_ammount=5):
    data = np.abs(data)
    values = []
    heights = []
    last_values = {}
    size = len(data)

    # Joining data with past values
    # Uses simple average
    for index, value in enumerate(data):
        if index not in last_values:
            last_values[index] = [0] * filter_ammount
        last_values[index][0] = value
        last_values[index] = np.roll(last_values[index], -1)
        final = np.average(last_values[index])
        heights.append(int(1.5 * final) + 50)

    return heights

class Bar(Widget):
    value = NumericProperty(0)

class PDS(App):
    title = 'PDS'
    song_name = 'Otherside'
    chunk_size = 1024
    filter_ammount = 5
    bar_spacing = NumericProperty(2)
    bar_width = NumericProperty(1)
    input_type = [False, False, False]
    scale_type = ListProperty([False, False])

    render_data = {}
    draw_index = 0

    playing = BooleanProperty(False)
    progress = NumericProperty(0)
    colors = DictProperty({
        'alpha': [0, 0, 0, 0],
        'white': [1, 1, 1, 1],
        'off_white': [0.91, 0.91, 0.91, 1],
        'black': [0, 0, 0, 1],
        'grey': [0.2, 0.2, 0.2, 1],
    })

    def build(self):
        root = PDSRoot()
        root.app = self
        self.root = root
        return root

    def song_check(self):
        FILENAME = './assets/wav/' + self.song_name + '.wav'
        # import pdb; pdb.set_trace()

        wav_file = wave.open(FILENAME, "rb")
        if wav_file.getnchannels() > 1:
            print("File is stereo. Converting...")
            sound = AudioSegment.from_wav(FILENAME)
            sound = sound.set_channels(1)
            sound.export(FILENAME, format="wav")
            wav_file.close()
            wav_file = wave.open(FILENAME, "rb")
        return wav_file.getnchannels() == 1

    def render(self, song_name='', output=False, zip=False):
        if song_name:
            self.song_name = song_name
        if not self.song_check():
            print('Failed to render audio visualization. Unable to convert song to single channel.')

        if os.path.exists(os.path.abspath('./assets/outputs/' + self.song_name + '.pds')):
            print('Render already exists.')
            return

        FILENAME = './assets/wav/' + self.song_name + '.wav'
        final_data = {'chunk_size': self.chunk_size, 'fft_heights':[]}

        file_size = 0
        with open(FILENAME, 'r') as f:
            file_size = os.fstat(f.fileno()).st_size

        wav_file = wave.open(FILENAME, "rb")
        byte_data = wav_file.readframes(self.chunk_size)
        count = 0
        while len(byte_data) == self.chunk_size * 2:
            data = np.frombuffer(byte_data, dtype='<i2')
            final_data['fft_heights'].append([byte_data, get_fft(data)])
            byte_data = wav_file.readframes(self.chunk_size)
            count += self.chunk_size * 2
            print(count * 100.0 / file_size)
        wav_file.close()

        if output:
            if zip:
                with gzip.open('./assets/outputs/' + self.song_name + '.zip', 'w') as f:
                    pickle.dump(final_data, f)
            else:
                pickle.dump(final_data, open('./assets/outputs/' + self.song_name + '.pds', 'wb'))
        self.render_data = final_data

    def load_render(self, zip=False):
        def _load(zip):
            data = self.render_data
            if not data:
                data = ''
                if zip:
                    with gzip.open('./assets/outputs/' + self.song_name + '.zip', 'rb') as f:
                        data = pickle.load(f)
                else:
                    with open('./assets/outputs/' + self.song_name + '.pds', 'rb') as f:
                        data = pickle.load(f)

            wav_file = wave.open('./assets/wav/' + self.song_name + '.wav', "rb")
            self.chunk_size = data['chunk_size']
            self.frequencies = [wav_file.getframerate()/self.chunk_size * (i+1) for i in range(self.chunk_size)]

            pya = pyaudio.PyAudio()
            stream = pya.open(
            format=pyaudio.paInt16,
            channels=1,
            rate=wav_file.getframerate(),
            frames_per_buffer=2*self.chunk_size,
            output=True
            )
            stream.start_stream()
            self.playing = True
            self.set_status(True)

            interval = 1.0 * self.chunk_size / wav_file.getframerate()
            t0 = time.time()
            exec_time = 0
            index = 0
            size = len(data['fft_heights'])
            byte_data = data['fft_heights'][index][0]
            while stream.is_active() and self.playing:
                if stream.get_write_available() >= self.chunk_size and index < size and len(byte_data) == 2 * self.chunk_size:
                    exec_time += index * interval
                    self.title = '{} - {:.2f}s'.format(self.song_name, exec_time)

                    byte_data = data['fft_heights'][index][0]
                    fft_data = data['fft_heights'][index][1]

                    # import pdb; pdb.set_trace()
                    fft_data = self.apply_filters(fft_data)
                    bar_data = fft_to_bar(fft_data[:int(len(fft_data) * 0.41)], filter_ammount=self.filter_ammount)
                    ifft_data = get_inverse_fft(fft_data)

                    filtered_data = ''
                    for item in ifft_data:
                        if item > 32767:
                            item = 32767
                        elif item < - 32767:
                            item = - 32767

                        filtered_data = filtered_data + struct.pack('<h', item)

                    stream.write(filtered_data)

                    index += 1
            self.title = 'PDS'

            stream.stop_stream()
            stream.close()
            wav_file.close()
            pya.terminate()
            self.set_status(False)

        if not self.playing:
            self.title = 'Carregando: {}'.format(self.song_name)
            self.player_thread = Thread(target=_load, args=[zip])
            self.player_thread.daemon = True
            self.player_thread.start()

    def stream_music(self):
        def _start_stream():
            if not self.song_check():
                print('Unable to convert song to single channel.')
                return

            FILENAME = './assets/wav/' + self.song_name + '.wav'
            wav_file = wave.open(FILENAME, "rb")
            self.frequencies = [wav_file.getframerate()/self.chunk_size * (i+1) for i in range(self.chunk_size)]

            pya = pyaudio.PyAudio()
            stream = pya.open(
            format=pyaudio.paInt16,
            channels=1,
            rate=wav_file.getframerate(),
            frames_per_buffer=2 * self.chunk_size,
            output=True
            )
            stream.start_stream()
            self.playing = True
            self.set_status(True)

            interval = 1.0 * self.chunk_size / wav_file.getframerate()
            exec_time = 0
            index = 0
            byte_data = wav_file.readframes(self.chunk_size)
            while stream.is_active() and len(byte_data) == 2 * self.chunk_size and self.playing:
                if stream.get_write_available() >= self.chunk_size:
                    exec_time = index * interval
                    self.title = '{} - {:.2f}s'.format(self.song_name, exec_time)

                    int_data = np.frombuffer(byte_data, dtype='<i2')
                    # import pdb; pdb.set_trace()
                    fft_data = self.apply_filters(get_fft(int_data))
                    ifft_data = get_inverse_fft(fft_data)
                    bar_data = fft_to_bar(fft_data[:int(len(fft_data) * 0.41)], filter_ammount=self.filter_ammount)

                    filtered_data = b''
                    for item in ifft_data:
                        if item > 32767:
                            item = 32767
                        elif item < - 32767:
                            item = - 32767
                        # import pdb; pdb.set_trace()
                        filtered_data = filtered_data + struct.pack('<h', item)
                    stream.write(filtered_data)
                    self.draw(bar_data)

                    byte_data = wav_file.readframes(self.chunk_size)
                    index += 1

            self.title = 'PDS'
            stream.stop_stream()
            stream.close()
            pya.terminate()

        self.player_thread = Thread(target=_start_stream, args=[])
        self.player_thread.daemon = True
        self.player_thread.start()

    def stream_mic(self):
        def _start_stream():

            self.frequencies = [44100/self.chunk_size * (i+1) for i in range(self.chunk_size)]

            pya = pyaudio.PyAudio()
            stream = pya.open(
            format=pyaudio.paInt16,
            channels=1,
            rate=44100,
            frames_per_buffer=self.chunk_size,
            input=True
            )
            stream.start_stream()
            self.set_status(True)

            interval = 1.0 * self.chunk_size / 44100
            exec_time = 0
            index = 0
            byte_data = stream.read(self.chunk_size)
            while stream.is_active() and self.playing:
                exec_time = index * interval
                self.title = 'Microfone - {:.2f}s'.format(exec_time)

                int_data = np.frombuffer(byte_data, dtype='<i2')
                fft_data = self.apply_filters(get_fft(int_data))
                ifft_data = get_inverse_fft(fft_data)
                bar_data = fft_to_bar(fft_data[:int(len(fft_data) * 0.41)], filter_ammount=self.filter_ammount)

                self.draw(bar_data)

                byte_data = stream.read(self.chunk_size)
                index += 1

            self.title = 'PDS'
            stream.stop_stream()
            stream.close()
            pya.terminate()

        self.player_thread = Thread(target=_start_stream, args=[])
        self.player_thread.daemon = True
        self.player_thread.start()

    @mainthread
    def draw(self, bar_data):
        spacing = self.bar_spacing
        mode = 'linear' if self.scale_type[0] else 'log'
        if mode == 'log':
            spacing = 500

        if len(self.root.ids.bars_screen.bars) == 0:
            for index, height in enumerate(bar_data):
                if mode == 'log':
                    x = int(spacing * np.log10(self.frequencies[index])) + index - 661
                elif mode == 'linear':
                    x = spacing * index + index
                bar = Bar(width=self.bar_width, x=x)
                bar.value = height
                self.root.ids.bars_screen.bars.append(bar)
                self.root.ids.bars_screen.rl.add_widget(bar)
            Window.size = x, Window.height
        else:
            for index, height in enumerate(bar_data):
                self.root.ids.bars_screen.bars[index].value = height

    def update_bars_configuration(self):
        mode = 'linear' if self.scale_type[0] else 'log'
        spacing = self.bar_spacing
        x = 0
        for index, bar in enumerate(self.root.ids.bars_screen.bars):
            if mode == 'linear':
                x = spacing * index
            elif mode == 'log':
                spacing = 500
                x = int(spacing * np.log10(self.frequencies[index])) - 661
            if index > 0:
                x += index * self.bar_width
            bar.x = x
            bar.width = self.bar_width
        Window.size = x or 400, Window.height

    def apply_filters(self, fft_data):
        current_filter = 0
        filters = self.root.ids.bars_screen.get_filters()
        # import pdb; pdb.set_trace()
        weights = []
        for index, fft in enumerate(fft_data):
            total = 0
            for filter in filters:
                total += filter.get_windowed_value(index)
            weights.append(total)

        weights = np.array(weights)
        return np.multiply(fft_data, weights)

    def on_bar_width(self, instance, value):
        self.update_bars_configuration()

    def on_bar_spacing(self, instance, value):
        self.update_bars_configuration()

    def on_scale_type(self, instance, value):
        self.update_bars_configuration()

    @mainthread
    def set_status(self, value):
        self.playing = value

    def toggle_player(self):
        if not self.playing:
            self.do_play()
        else:
            self.do_pause()

    def do_pause(self):
        self.set_status(False)

    def do_play(self):
        render, real_time, mic = self.input_type
        if render:
            self.render(song_name=self.song_name, output=True)
            self.load_render()
        elif real_time:
            self.stream_music()
        elif mic:
            self.stream_mic()


class PDSRoot(RelativeLayout):
    def on_touch_down(self, touch):
        return super(PDSRoot, self).on_touch_down(touch)

if __name__ == '__main__':
    PDS().run()
